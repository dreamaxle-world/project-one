<?php
header('HTTP/1.1 200 OK');
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?='PHP'?> Title</title>
</head>
<body>
Hello World from PHP
<?php
print_r(parse_ini_file("php.ini"));
$servername = "localhost";
$username = "username";
$password = "userpassword";

try {
    $conn = new PDO("mysql:host=$servername;dbname=myDB", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
?>
</body>
</html>