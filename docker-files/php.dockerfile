FROM php:7.4-fpm
RUN apt-get update -yqq && apt-get install -yqq libpng-dev libjpeg62-turbo-dev libwebp6 libfreetype6-dev libwebp-dev libxpm-dev libicu-dev libxml2-dev  libonig-dev
RUN pecl install -f xdebug
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN echo 'memory_limit=512M' > "$PHP_INI_DIR/conf.d/memory.ini"


RUN echo 'upload_max_filesize = 64M' > "$PHP_INI_DIR/conf.d/uploads.ini"
RUN echo 'post_max_size = 64M' >> "$PHP_INI_DIR/conf.d/uploads.ini"

#file_uploads = On
#memory_limit = 64M
#upload_max_filesize = 64M
#post_max_size = 64M
#max_execution_time = 600

RUN export XDEBUG_PATH=`find / -name "xdebug.so"` && \
    echo "zend_extension=$XDEBUG_PATH" > /usr/local/etc/php/conf.d/ext-xdebug.ini && \
    echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/ext-xdebug.ini && \
    echo "xdebug.client_host=172.19.0.1">> /usr/local/etc/php/conf.d/ext-xdebug.ini && \
    echo "xdebug.client_port=9001" >> /usr/local/etc/php/conf.d/ext-xdebug.ini


RUN docker-php-ext-install -j$(nproc) intl
RUN docker-php-ext-install -j$(nproc) opcache
RUN docker-php-ext-install -j$(nproc) pdo_mysql
#RUN docker-php-ext-install -j$(nproc) exif
RUN docker-php-ext-configure gd  --with-webp --with-jpeg   --with-xpm --with-freetype
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install -j$(nproc) xml
RUN docker-php-ext-install -j$(nproc) mbstring
