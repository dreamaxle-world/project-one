FROM ubuntu:latest
RUN echo 'message mysql.dockerfile'
RUN DEBIAN_FRONTEND=noninteractive
RUN apt-get upgrade
RUN apt-get update
RUN apt-get install -y wget
RUN apt-get install -y mysql-server
CMD chown -R mysql:mysql /var/lib/mysql \
&& service mysql start \
&& mysql -ppassword -e "CREATE DATABASE IF NOT EXISTS start_database;GRANT ALL PRIVILEGES on start_database.* TO 'username'@'localhost' WITH GRANT OPTION; ALTER USER 'username'@'localhost' IDENTIFIED WITH mysql_native_password BY 'userpassword';FLUSH PRIVILEGES;SET GLOBAL connect_timeout=28800;SET GLOBAL sql_mode='NO_ENGINE_SUBSTITUTION';" \